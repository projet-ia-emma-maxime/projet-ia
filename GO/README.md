## Projet de GO

Ce projet vise à créer un joueur de GO capable de battre d'autres joueurs.
Le joueur doit pouvoir s’intégrer à un tournoi entre joueurs.

###Contributeurs :

***Equipe 13296***

    - Maxime GIRARDET
    - Emma ROBIN

Nous avons implémenté *alphabeta* avec *iterative deepening* en tant que stratégie de notre joueur.
Nous commençons la recherche avec une profondeur de 4, puis nous calculons le temps de jeu restant pour notre joueur et décrémentons la profondeur dès lors que le temps se réduit selon une certaine échelle présentée ci-après:
* Reste entre et 4 et 8 min : **profondeur = 4**
* Reste entre 30 sec et 4 min : **profondeur = 3**
* Reste moins de 30 sec : **profondeur = 2**

On interrompt la recherche si celle-ci dépasse 60 sec (avec une profondeur de 4), ce délai est un attribut de la classe.

Concernant la bibliothèque d'ouverture, elle se base sur les mouvements les plus couramment joués en premier afin d'optimiser les chances de gain.
Ceux-ci proviennent de cette [page web](https://en.wikipedia.org/wiki/Go_strategy_and_tactics#Go_opening_theory).

