## TP noté de Machine Learning

Ce TP noté vise à déployer des méthodes d'apprentissage automatiques permettant d'évaluer la qualité de plateaux de GO.

###Contributeurs :

***Equipe 13296***

    * Maxime GIRARDET
    * Emma ROBIN

### Usage

Un fichier *Jupyter* ``tp-ml-go.ipynb`` contient l'ensemble des explications (cellules de texte) et du code permettant de réaliser l'apprentissage sur un jeu de données d'entraînement et les prédictions sur un jeu de données distinct.

Ce fichier est disponible au format HTML (``tp-ml-go.html``) , ce qui permet d'observer les résultats sans réexécution.

Enfin, un fichier texte (``my_predictions.txt``)  contient l'ensemble des prédictions pour le set de données final.

### Implémentation

Nous avons testé différentes fonctions de *loss*, *d'optimisation* et *d'activation*, celles fournissant les meilleures performances ont été retenues :
* Fonction d'activation : **ReLU**
* Fonction de loss : **MeanSquaredError**
* Fonction d'optimisation : **RMSprop** avec le paramètre *learning rate* à 0.001

Lors de la préparation de nos données, nous avons effectué une rotation (`rot90`) et une inversion des lignes (`flipud`) de chaque plateau.
Cela permet d'augmenter le set de données disponibles. Nous assurons que la probabilité associée à ces symétries est la même que le plateau original.
Nous avons tenté de vérifier que les plateaux symétriques ajoutés ne sont pas déjà dans la liste des plateaux. Néanmoins, la complexité temporelle est très importante et rend la génération trop longue pour être exploitée.
De plus, sur de petites valeurs (jusqu'à 1000 plateaux), il n'y avait pas de doublons détectés.

Concernant l'entrainement en lui-même, nous utilisons un modèle séquentiel composé de 3 couches de convolution avec des nombres de neurones en sortie croissants.
Nous aplatissons ensuite les données grâce à `flatten` afin de pouvoir appliquer 3 couches `Dense`.

Nous avons veillé à ne pas effectuer de surapprentissage en essayant de minimiser la *loss* de validation tout en s'assurant qu'elle ne soit pas supérieure à la *loss* d'entraînement.

Afin de définir le nombre de neurones à utiliser, nous avons généré un graphe permettant d'effectuer la comparaison de la *loss* de validation et de la *loss* d'entraînement avec un très grand nombre de neurones.

![Graphe de comparaison](graphs/index.png)

Comme nous pouvons le voir sur le graphe, nous atteignons très rapidement l'intersection avec la *loss* d'entrainement.
Inversement, si nous ne mettons pas assez de mémoire, le point d'intersection est atteint au bout d'un très grand nombre d'*epochs*.

Nous avons donc opté pour un nombre de neurones intermédiaire. Voici le graphe que nous obtenons :

![Graphe de comparaison](graphs/graph2.png)

Nous voyons donc qu'il faut arrêter l'entrainement au bout d'environ 20 *epochs*.

